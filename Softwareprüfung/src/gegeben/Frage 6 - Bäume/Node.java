public class Node {
    public char value;
    public Node left;
    public Node right;

    public Node(char value, Node left, Node right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public Node(char value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public boolean f(char value) {
        if (this.value == value) {
            return true;
        }
        if (left != null) {
            if (left.f(value)) {
                return true;
            }
        }
        if (right != null) {
            if (right.f(value)) {
                return true;
            }
        }
        return false;
    }
}