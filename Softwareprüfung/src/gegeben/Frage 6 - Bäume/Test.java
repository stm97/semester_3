
public class Test {

    public static void main(String[] args) {
        Node n1 = new Node('a');
        Node n2 = new Node('d');
        Node n3 = new Node('b', n1, n2);
        Node n4 = new Node('g');
        Node n5 = new Node('f', n3, n4);

        System.out.println(n1.f('a'));
        System.out.println (n1.f('c'));

        System.out.println(n5.f('a'));
        System.out.println (n5.f('c'));
    }
}