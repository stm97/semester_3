import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ZahlTest {

	Zahl z;

	@BeforeEach
	void setUp() {
		z = new Zahl();
	}

	@Test
	void testVerdoppeln() {
		double input = 100;
		z.setWert(input);
		double expected = 200;
		double actual = z.verdopple();
		assertEquals(expected, actual);
		assertEquals(expected, z.getWert());
	}

	@Test
	void testHalbieren() {
		double input = 100;
		z.setWert(input);
		double expected = 50;
		double actual = z.halbiere();
		assertEquals(expected, actual);
		assertEquals(expected, z.getWert());
	}

	@Test
	void testQuadrieren1() {
		double input = 5;
		z.setWert(input);
		double expected = 25;
		double actual = z.quadriere();
		assertEquals(expected, actual);
		assertEquals(expected, z.getWert());
	}

	@Test
	void testQuadrieren2() {
		assertEquals(1, z.quadriere());
	}

	@Test
	void testWurzelZiehen1() {
		double input = 49;
		z.setWert(input);
		double expected = 7;
		double actual = z.wurzel();
		assertEquals(expected, actual);
		assertEquals(expected, z.getWert());
	}

	@Test
	void testWurzelZiehen2() {
		assertEquals(1, z.wurzel());
	}
}
