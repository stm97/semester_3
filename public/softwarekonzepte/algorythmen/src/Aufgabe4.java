class Aufgabe4 {
    private int sum = 0;
    Aufgabe4(int n){
        methodeEins(n);
        methodeZwei(n);
        methodeDrei(n);
    }

    private void methodeEins(int n){
        sum = 0;
        for(int i=0;i<n; i++){
            sum++;
        }
        System.out.println("Aufgabe eins fuer n="+ n +" ist " + sum);
    }

    private void methodeZwei(int n){
        sum = 0;
        for(int i=0;i<n;i++){
            for (int j=0; j<n; j++){
                sum++;
            }
        }
        System.out.println("Aufgabe zwei fuer n="+ n +" ist " + sum);
    }

    private void methodeDrei(int n){
        for (int i=0; i<n; i++){
            for (int j=0; j<i; j++){
                sum++;
            }
        }
        System.out.println("Aufgabe drei fuer n="+ n +" ist " + sum);
    }

}
