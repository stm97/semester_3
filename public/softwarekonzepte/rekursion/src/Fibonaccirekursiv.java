public class Fibonaccirekursiv {
    public static void main(String[] args) {
        int n = 45;
        int b =fib(n);

        System.out.println("Die "+ n + "te Fibonaccizahl ist= "+ b);
    }

    private static int fib(int n){
        if (n==0 || n==1){
            return n;
        }
        else {
            return fib(n-1) + fib(n-2);
        }
    }
}
