public class Fibonacciiterativ {

    public static void main(String[] args) {
        double f0 = 1;
        double f1 = 1;
        double fn = 1;
        double zaehler = 2;

        while (zaehler<50){

            fn= f0 + f1;
            System.out.println("Die" + zaehler+ "te Zahl ist: " + fn);
            f0 = f1;
            f1 = fn;

            zaehler++;
        }
    }
}
