public class PascalDreieck {
    public static void main(String[] args) {
        int i = 5000000;
        int j = 3;
        int p = pascal(i, j);
        System.out.println("Die zahl an Stelle: " + i + ","+j+" ist: "+p);
    }

    private static int pascal(int i , int j){
        if (j==0 || j==i){
            return 1;
        }

        else{

            return pascal(i-1,j-1)+pascal(i-1, j);
        }
    }
}
