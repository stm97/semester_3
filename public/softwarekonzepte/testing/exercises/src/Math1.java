import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class Math1 {
    /**
     * Compute the maximum value of two values.
     * @param a first value
     * @param b second value
     * @return the maximum value
     */
    public static int max(int a, int b) {
        if (a>=b) {
            return a;
        } else {
            return b;
        }
    }

    public static boolean isEven(int a) {
        return a % 2 == 0;
    }
    @Test
    public void testMax(){
        assertEquals(10,Math1.max(10,3));
}
}