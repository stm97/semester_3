public class ComplexNumber {
    private double real;
    private double imaginary;

    public ComplexNumber(){
        real = 0.0;
        imaginary = 0.0;
    }
    public ComplexNumber(double real, double imaginary){
        this.real = real;
        this.imaginary = imaginary;
    }

    public double getReal(){
    return real;
    }

    public double getImaginary(){
        return imaginary;
    }

    public ComplexNumber add(ComplexNumber a){
        ComplexNumber c = new ComplexNumber();
        c.real = real+a.getReal();
        c.imaginary = imaginary+a.getImaginary();

        return c;
    }

    public double abs(){
    double result = java.lang.Math.sqrt(real*real + imaginary*imaginary);
    return result;
    }

    public String toString(){
        String a = "";
        if (imaginary>=0){
            a = "+";
        }
    String result = real +a+ imaginary+ "i";
    return result;
    }
}
