
public class Fakultaet {
    public static void main(String[] args) {
        fakultaet();
    }

    private static void fakultaet() {
        int n = 9;
        int fakultaet = 1;
        int i = 1;
        while (i <= n) {
            fakultaet *= i;
            i++;
        }
        System.out.println(fakultaet);
    }

}
