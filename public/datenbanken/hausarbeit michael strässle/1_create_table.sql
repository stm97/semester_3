CREATE TABLE tverlag (
verlagid INTEGER PRIMARY KEY,
verlagname VARCHAR (40),
strasse VARCHAR (30),
ort VARCHAR (30),
vornamekontakt VARCHAR (50),
nachnamekontakt VARCHAR (50),
mail VARCHAR (40),
telefon VARCHAR (20)
);

CREATE TABLE tbuch (
buchid INTEGER PRIMARY KEY,
titel VARCHAR (50),
isbn VARCHAR (20),
verlagid INTEGER REFERENCES tverlag(verlagid) ON DELETE RESTRICT,
sprache VARCHAR (20),
bestandlager INTEGER,
bestandgestell INTEGER,
bestelldauerday INTEGER ,
verfuegbarkeit BOOLEAN,

einkaufspreis INTEGER,
verkaufspreis INTEGER
 );

CREATE TABLE tautor (
autorid INTEGER PRIMARY KEY,
vorname VARCHAR (20),
nachname VARCHAR (20)
);

CREATE TABLE tbestellung (
bestellungid INTEGER PRIMARY KEY,
bestelldatum DATE,
anzahl INTEGER,
buchid INTEGER REFERENCES tbuch(buchid) ON DELETE RESTRICT,
bestellpreis INTEGER,
erhalten BOOLEAN
);

CREATE TABLE tbuch_tautor (
buchid INTEGER REFERENCES tbuch(buchid) ON DELETE RESTRICT,
autorid INTEGER REFERENCES tautor(autorid) ON DELETE RESTRICT
);
