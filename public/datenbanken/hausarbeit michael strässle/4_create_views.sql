CREATE VIEW vkaufinfos (titel,autor,sprache,preis) AS
SELECT b.titel as titel, STRING_AGG(a.nachname || ' ' || a.vorname, ' ,') as autor,
    b.sprache as sprache, b.verkaufspreis as preis
FROM tbuch as b, tautor as a, tbuch_tautor as ba
WHERE b.buchid = ba.buchid AND a.autorid = ba.autorid
GROUP BY b.titel,b.sprache,b.verkaufspreis;

CREATE VIEW vtelbestellung (isbn, verfuegbarkeit, bestelldauer, verlag, vornameansprech, nachnameansprech, telefon) AS
SELECT b.isbn as isbn, b.verfuegbarkeit as verfuegbarkeit, b.bestelldauerday as bestelldauer,
       v.verlagname as verlag, v.vornamekontakt as vornameansprech, v.nachnamekontakt as nachnameansprech,
       v.telefon as telefon
FROM tbuch as b, tverlag as v
WHERE b.verlagid = v.verlagid;

CREATE VIEW vmailbestellung (isbn, verfuegbarkeit, bestelldauer, verlag, vornameansprech, nachnameansprech, mail) AS
SELECT b.isbn as isbn, b.verfuegbarkeit as verfuegbarkeit, b.bestelldauerday as bestelldauer,
       v.verlagname as verlag, v.vornamekontakt as vornameansprech, v.nachnamekontakt as nachnameansprech,
       v.mail as mail
FROM tbuch as b, tverlag as v
WHERE b.verlagid = v.verlagid;

CREATE VIEW vwarenwert (warenwert) AS
SELECT SUM((b.bestandlager + b.bestandgestell)*b.verkaufspreis) as warenwert
FROM tbuch as b;

CREATE VIEW vverspaetet (bestellungid,bestelldatum,anzahl,buchid,bestellpreis,erhalten) AS
SELECT be.bestellungid as bestellungid, be.bestelldatum as bestelldatum, be.anzahl as anzahl,
    be.buchid as buchid, be.bestellpreis as bestellpreis, be.erhalten as erhalten
FROM tbuch as b, tbestellung as be
WHERE be.buchid = b.buchid AND (CURRENT_DATE - be.bestelldatum) > b.bestelldauerday AND be.erhalten = false;

CREATE VIEW vknapp (buchid,titel,bestandlager,bestandgestell) AS
SELECT DISTINCT b.buchid as buchid, b.titel as titel, b.bestandlager as bestandlager, b.bestandgestell as bestandgestell
FROM tbuch as b, tbestellung as be
WHERE b.bestandlager + b.bestandgestell < 5 AND (SELECT COUNT(*) FROM tbestellung WHERE b.buchid = be.buchid AND
                                                (CURRENT_DATE - be.bestelldatum) < 30) > 1;

CREATE VIEW valleine (titel,autor) AS
SELECT DISTINCT vba.titel as titel, vba.autor as autor FROM vkaufinfos as vba, tautor as a
WHERE vba.autor = (SELECT STRING_AGG(a.nachname || ' ' || a.vorname, ' ,') as sautor FROM tautor as a WHERE a.autorid = 10);

CREATE VIEW vverlag (titel, autor, verlag) AS
SELECT DISTINCT b.titel as titel, vba.autor as autor, v.verlagname as verlag FROM vkaufinfos as vba, tbuch as b, tbuch_tautor as ba, tverlag as v
WHERE b.verlagid = 1 AND v.verlagid = 1 AND b.buchid = ba.buchid AND b.titel = vba.titel;
