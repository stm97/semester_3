ALTER TABLE tbestellung ADD CONSTRAINT minanz CHECK(anzahl >= 1);
ALTER TABLE tbestellung ADD CONSTRAINT buchda CHECK (buchid NOTNULL);
ALTER TABLE tbestellung ADD CONSTRAINT datumcheck CHECK (bestelldatum <= CURRENT_DATE);
ALTER TABLE tbuch ADD CONSTRAINT bestl CHECK ( bestandlager NOTNULL );
ALTER TABLE tbuch ADD CONSTRAINT bestg CHECK ( bestandgestell NOTNULL );


