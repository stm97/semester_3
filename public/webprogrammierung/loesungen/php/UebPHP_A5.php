<!-- PHP, Aufgabe 5, GET-Parameter ausgeben U.Graf, 26.10.2011 -->
<html>
	<head>
		<title>Ausgabe des Query-Strings</title>
	</head>
	<body>
		<p>
			Der Query-String lautet: <?php echo $_SERVER['QUERY_STRING'];?>
		</p>
		<p>
			Folgende Werte wurden eingegeben:
		</p>
		<ul>
			<li>Text1z: <?php echo $_GET['Text1z'];?></li>
			<li>TextPassword: <?php echo $_GET['TextPassword'];?></li>
			<li>Textnz: <?php echo $_GET['Textnz'];?></li>
			<li>Checkbox: <?php echo $_GET['Checkbox'];?></li>
			<li>Liste: <?php echo $_GET['Liste'];?></li>
			<li>Radiobutton: <?php echo $_GET['Radiobutton'];?></li>
			<li>Combobox: <?php echo $_GET['Combobox'];?></li>
			<li>FileUpload: <?php echo $_GET['FileUpload'];?></li>
			<li>Button: <?php echo $_GET['Button'];?></li>
		</ul>
		<p>
			Eleganter geht es mit
		</p>
		<pre><?php print_r(array_values($_GET));?></pre>
		<p>
			Und phpinfo liefert nun:
		</p>
		<?php phpinfo() ?>
	</body>
</html>
