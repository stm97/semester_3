<!-- PHP, Aufgabe 4, U.Graf, 26.10.2011 -->
<html>
	<head>
		<title>var_dump</title>
	</head>
	<body>
		<p>
			<?php 
				$a=1.5;
				$b=17;
				$c=true;
				$d=array('a','b',$a,$b,array(1,2,3));
				var_dump($a); echo "<hr>";
				var_dump($b); echo "<hr>";
				var_dump($c); echo "<hr>";
				var_dump($d); echo "<hr>";
				var_dump($_SERVER); echo "<hr>";
				var_dump($x); echo "<hr>";
			?>
		</p>
	</body>
</html>
