<!-- PHP, Aufgabe 6, Formular pr�fen, U.Graf, 26.10.2011 -->
<html>
	<head>
		<title>Formularverifikation (Serverseitig)</title>
		<style>
			.error { color: red; }
			.required { background-color: #ffc0ff; }
		</style>
<?php 
	if (isset($_GET['state']) && $_GET['state'] == 'sent') {
//		phpinfo();
		$checkInput=true;
		// Anrede testen: Muss vorhanden sein
		if(isset($_GET['Radiobutton']) && (($_GET['Radiobutton'] == "AnredeHerr") || ($_GET['Radiobutton'] == "AnredeFrau"))) {
			$anredeOk=true;
		} else {
			$anredeOk=false;
		}
		// Name testen: Mindestens 3 Zeichen lang
		if(strlen($_GET['Name'])>=3) {
			$nameOk=true;
		} else {
			$nameOk=false;
		}
		// Alter: falls vorhanden, positive Ganzzahl
		$alterOk=true;
		if($_GET['Alter'] != "") {
			$alter=(int)$_GET['Alter']; // Typumwandlung String in Zahl
			if (!is_int($alter) || ($alter<=0)) $alterOk=false;
		}
		// Sprachen: mindestens 1
		$sprachenOk=true;
		if(empty($_GET['Sprache'])) $sprachenOk=false;
		//Bemerkungen optional, max. 50 Zeichen
		$bemerkungenOk=true;
		if(strlen($_GET['Bemerkungen'])>50) {
			$bemerkungenOk=false;
		}
	} else {
		$checkInput=false;
		}
?>
	</head>

	<body>
		<pre><?php print_r(array_values($_GET));?></pre>
		<form action="<?php echo ($_SERVER['PHP_SELF']); ?>" method="get">
			<h1>Serverseitige �berpr�fung von Formularen</h1>			
	
			<table border=1>
				<tr>
					<td class="required">Anrede</td>
					<td>
						<input type="radio" name="Radiobutton" value="AnredeFrau"  <?php if (isset($_GET['Radiobutton']) && $_GET['Radiobutton'] == "AnredeFrau") echo "checked"; ?> >Frau
						<input type="radio" name="Radiobutton" value="AnredeHerr"  <?php if (isset($_GET['Radiobutton']) && $_GET['Radiobutton'] == "AnredeHerr") echo "checked"; ?> >Herr
					</td>
					<td class="error">
					 <?php if($checkInput && !$anredeOk) echo 'Einer der Werte muss gesetzt werden!'; ?>
					</td>
				</tr>
				<tr>
					<td class="required">Name</td>
					<td><input type="text" name="Name" value="<?php if (isset($_GET['state'])) echo $_GET['Name']; ?>"></td>
					<td class="error">
					 <?php if($checkInput && !$nameOk) echo 'Name muss mindestens 3 Zeichen umfassen!'; ?>
					</td>
				</tr>
				<tr>
					<td <?php if($checkInput && !$alterOk) echo 'class="error"'; ?> >Alter</td>
					<td><input type="text" name="Alter" value="<?php if (isset($_GET['state'])) echo $_GET['Alter']; ?>"></td>
					<td class="error">
					 <?php if($checkInput && !$alterOk) echo 'Muss positive Ganzzahl sein!'; ?>
					</td>
				</tr>
				<tr>
					<td>Hobbies</td>
					<td>
						<select name="Hobbies[]" multiple size=3>
							<option value=l1 <?php if(isset($_GET['Hobbies']) && in_array('l1',$_GET['Hobbies'])) echo 'selected'; ?>>Klettern</option>
							<option value=l2 <?php if(isset($_GET['Hobbies']) && in_array('l2',$_GET['Hobbies'])) echo 'selected'; ?>>Skifahren</option>
							<option value=l3 <?php if(isset($_GET['Hobbies']) && in_array('l3',$_GET['Hobbies'])) echo 'selected'; ?>>Musik</option>
							<option value=l4 <?php if(isset($_GET['Hobbies']) && in_array('l4',$_GET['Hobbies'])) echo 'selected'; ?>>Fotografieren</option>
							<option value=l5 <?php if(isset($_GET['Hobbies']) && in_array('l5',$_GET['Hobbies'])) echo 'selected'; ?>>Segeln</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="required">Sprachen</td>
					<td>
						<input type=checkbox name="Sprache[]" value="d" <?php if (isset($_GET['Sprache']) && in_array('d',$_GET['Sprache'])) echo "checked"; ?> >Deutsch<br>
						<input type=checkbox name="Sprache[]" value="f" <?php if (isset($_GET['Sprache']) && in_array('f',$_GET['Sprache'])) echo "checked"; ?> >Franz�sisch<br>
						<input type=checkbox name="Sprache[]" value="e" <?php if (isset($_GET['Sprache']) && in_array('e',$_GET['Sprache'])) echo "checked"; ?> >Englisch<br>
						<input type=checkbox name="Sprache[]" value="i" <?php if (isset($_GET['Sprache']) && in_array('i',$_GET['Sprache'])) echo "checked"; ?> >Italienisch<br>
						<input type=checkbox name="Sprache[]" value="s" <?php if (isset($_GET['Sprache']) && in_array('s',$_GET['Sprache'])) echo "checked"; ?> ">Spanisch<br>
					</td>
					<td class="error">
					 <?php if($checkInput && !$sprachenOk) echo 'Mindestens 1 Sprache!'; ?>
					</td>
				</tr>
				<tr>
					<td <?php if($checkInput && !$bemerkungenOk) echo 'class="error"'; ?> >Bemerkungen</td>
					<td><textarea name="Bemerkungen"><?php if (isset($_GET['state'])) echo $_GET['Bemerkungen']; ?></textarea></td>
					<td class="error">
					 <?php if($checkInput && !$bemerkungenOk) echo 'Maximal 50 Zeichen!'; ?>
					</td>
				</tr>

				<tr>
					<td><input type="hidden" name="state" value="sent" /></td>
					<td><input type=reset value="Zur�cksetzen"><input type=submit value="   Senden   "></td>
				</tr>

			</table>
		</form>
	</body>
</html>
