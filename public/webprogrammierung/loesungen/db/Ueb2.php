<!-- PHP, Aufgabe 5, GET-Parameter ausgeben U.Graf, M. Lehmann -->
<html>
	<head>
		<title>MySQL Anbindung mit mysqli</title>
	</head>
	<body>
		connect:
		<?php
		mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX);
	try {

	$mysqli = new mysqli("inf002.ntb.ch", "webkurs", "srukbew", "webcourse");
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	echo $mysqli->host_info . "\n";

	$res = $mysqli->query("SELECT id, username AS uname, email, password, age FROM users ORDER BY id ASC");
	#$res = $mysqli->query("SHOW TABLES");

	echo "<br>";

    echo "<table><tr> <th>ID</th> <th>username</th> <th>email</th>  <th>password (hash)</th>  <th>age</th> </tr>";
    // output data of each row
    while($row = $res->fetch_assoc()) {
        echo "<tr><td>".$row["id"]."</td><td>".$row["uname"]. "</td><td>" .$row["email"]. "</td><td>" .$row["password"]. "</td><td>" .$row["age"]."</td></tr>";
    }
    echo "</table>";

	echo 	"<br> <hr>";

	}
	catch (Exception $e){
		echo "Es ist eine Exception aufgetreten <br>";
		echo $e->getMessage();
		
	}

		?>
		<p>
			phpinfo:
		</p>
		<?php phpinfo() ?>
	</body>
</html>
