<?php
	//get parameters
	$dbserver = $_GET['dbserver'];
	$dbuser = $_GET['dbuser'];
	$dbpwd = $_GET['dbpwd'];
	$db = $_GET['db'];
	$table=$_GET['table'];
	if (isset($_GET['order'])) $orderattribute=$_GET['order'];
	if (isset($_GET['dir'])) $orderdirection=$_GET['dir'];
	
	$parameter="dbserver=$dbserver&dbuser=$dbuser&dbpwd=$dbpwd&db=$db&table=$table";
	
	// ***********************************************************
	// Hinweis: Diese L�sung verwendet eine Vorgaengertechnologie f�r den DB Zugriff:
	// https://www.php.net/manual/en/function.mysql-connect.php
	// ***********************************************************
	
	//Connect to mysql database server; in case of error load login-form again
	//@ suppresses the php-internal warning message
	if (!@mysql_connect($dbserver, $dbuser, $dbpwd)) {
		header("location:UebDB_A5.htm");
		exit();
	}
	
	//select database
	if (!@mysql_select_db($db)) {
		die("-$db- " . mysql_error());
		header("location:UebDB_A5.htm");
		exit();
	}
?>
<html>
	<!-- U.Graf, 16.11.2011 -->
  <head>
    <title>
      Darstellung der Tabelle '
      <?php echo $table; ?>
      ' mit Metadaten
    </title>
  </head>
  
  <body>
    <h1>Darstellung der Tabelle '
      <?php echo $table; ?>
      ' mit Metadaten</h1>
<?php
				//get metainformation about table
				$query = "select * from $table";
				$result = mysql_query($query);
				$columns = mysql_num_fields($result);
				
				//build query
				$query = "select * from $table where 1";
				if (isset($orderattribute)) $query = $query . " order by $orderattribute $orderdirection";
				
				//execute query
				$result = mysql_query($query);
				if (!$result) {
					echo("<p>Die Anfrage '$query' enth�lt einen Fehler: " . mysql_error() . "</p>");
					return;
				}      
				echo("<p>Die Anfrage '$query' liefert folgendes Ergebnis:</p>");
    ?>
    <form action="<?php echo($PHP_SELF); ?>" method="get">
      <input type="hidden" name="dbserver" value="<?php echo($dbserver); ?>">
      <input type="hidden" name="dbuser" value="<?php echo($dbuser); ?>">
      <input type="hidden" name="dbpwd" value="<?php echo($dbpwd); ?>">
      <input type="hidden" name="db" value="<?php echo($db); ?>">
      <input type="hidden" name="table" value="<?php echo($table); ?>">
      <input type="hidden" name="order" value="<?php echo($orderattribute); ?>">
      <input type="hidden" name="dir" value="<?php echo($orderdirection); ?>">
      
      <table border="1">
        <tr>
          <!-- build up header row -->
<?php
# Hinweis: Die Daten werden Serverseitig sortiert!
					for ($i = 0; $i < $columns; $i++) {
						$fieldname = mysql_field_name($result, $i);
						print("<th>$fieldname
							<a href=\"Ueb5_Tbl.php?$parameter&order=$fieldname&dir=asc\">&uarr;</a> 
							<a href=\"Ueb5_Tbl.php?$parameter&order=$fieldname&dir=desc\">&darr;</a></th>");
					}
          ?>
        </tr>
        <!-- data -->
<?php
					while ($row = mysql_fetch_array($result)) {
						print("<tr>");
						for ($i = 0; $i < $columns; $i++) {print("<td>"); print($row[$i]); print("</td>");}
						print("</tr>\n");
					}
        ?>
      </table>
    </form>
  </body>
</html>
