<!-- PHP, Aufgabe 5, GET-Parameter ausgeben U.Graf, M. Lehmann -->
<html>
	<head>
		<title>MySQL Anbindung mit mysqli</title>
	</head>
	<body>
		connect:
		<?php
		mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX);
	try {

	$mysqli = new mysqli("inf002.ntb.ch", "webkurs", "srukbew", "webcourse");
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	echo $mysqli->host_info . "\n";

	$res = $mysqli->query("SELECT id, username AS uname, email FROM users ORDER BY id ASC");

	echo "<br>";
	while ($row = $res->fetch_assoc()) {
		#Zugriff via Spaltenname oder ALIAS (username AS uname)
    echo " id = " . $row['id'] .  ", username = " . $row['uname'] .", email = " . $row['email'] .   "<br>";
	
	}
	echo 	"<br> <hr>";

	}
	catch (Exception $e){
		echo "Es ist eine Exception aufgetreten <br>";
		echo $e->getMessage();
		
	}

		?>
		<p>
			phpinfo:
		</p>
		<?php phpinfo() ?>
	</body>
</html>
