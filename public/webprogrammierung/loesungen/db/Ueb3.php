<!-- PHP, Aufgabe 5, GET-Parameter ausgeben U.Graf, M. Lehmann -->
<html>
	<head>
		<title>MySQL Anbindung mit mysqli</title>
	</head>
	<body>
		connect:
		<?php
		mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX);
	try {

	$mysqli = new mysqli("inf002.ntb.ch", "webkurs", "srukbew", "webcourse");
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	echo $mysqli->host_info . "\n";
	echo "<hr>";

    // output all data
	echo "All tables: <br>";

	// V1
	$res = $mysqli->query("SHOW TABLES");
	while ($row = $res->fetch_row()) {
	echo  "TableName: " . $row[0] . "<br>";
	}
	
	// V2
	echo "<br> print_r() in pre (fetch_all ohne while): <br>";
	$res = $mysqli->query("SHOW TABLES");
	echo "<pre>";
	echo print_r($res->fetch_all());
	echo "</pre>";
	
	// V3
	echo "<br> print_r() in pre (fetch_array, wile): <br>";
	$res = $mysqli->query("SHOW TABLES");
	echo "<pre>";
	while ($row = $res->fetch_array()) {
	echo  print_r($row);
	}
	echo "</pre>";
	
	
	
	echo 	"<br> <hr>";

	}
	catch (Exception $e){
		echo "Es ist eine Exception aufgetreten <br>";
		echo $e->getMessage();
		
	}

		?>
		<p>
			phpinfo:
		</p>
		<?php phpinfo() ?>
	</body>
</html>
