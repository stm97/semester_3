<?php
// local var. not in session, not in cookie:
$input_is_valid = false;
$fiNa = $laNa = "";


// if present, set the variables to the posted values
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (!empty($_POST["firstname"])) {
	  $fiNa = $_POST["firstname"];
  }
  if (!empty($_POST["lastname"])) {
	  $laNa = $_POST["lastname"];
  }
// Note: this is a reduced exercise. In a full application, one needs to perform serverside validation!
$input_is_valid = true;
}

if ($input_is_valid){
setcookie("fiName", $fiNa, time() + (86400 * 30), "/"); // 86400s = 1 Tag
setcookie("laName", $laNa, time() + 20, "/"); // nur 20 sekunden (zum Testen)
}


  
?>
<!DOCTYPE html>
<html>
<head>

<title>PHP Uebung mit Cookies</title>
</head>
<body>
<?php
	if(isset($_COOKIE["fiName"])) {
	echo "cookie fiName is set. Value = " .  $_COOKIE["fiName"]; }
	else {
		echo "cookie fiName is not set";
		}
	echo "<br>";
	
	if(isset($_COOKIE["laName"])) {
		echo "cookie laName is set. Value = " .  $_COOKIE["laName"];
	}
	else {
		echo "cookie laName is not set";
		}

		
 ?>

<h1>Eingabeformular</h1>


<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  First name:<br>
  <input type="text" name="firstname" minlength=0 maxlength=50 required value="<?php echo $fiNa ;?>" ><br>
  Last name:<br>
  <input type="text" name="lastname" minlength=2 maxlength=50 required value="<?php echo $laNa; ?>" ><br><br>
  <input type="submit" value="Anmelden">
</form>

<p>
Sobald man Anmelden (submit) klickt, werden die Daten per POST an den Server gesandt. Der Server erstellt dann zwei Cookies,
welche dann beim Cient gespeichert werden und beim naechsten Aufruf wieder vom Client an den Server gesandt werden. 
Beachten Sie, dass die beiden cookies unterschiedliche Ablaufdaten haben. 
</p>

<?php
  if ($input_is_valid){
	  echo " <p> ";
	  echo "Mit Weiter geht es auf eine neue Seite. Die eingegebenen Informationen werden per Cookie zwischen Client und Server hin und her gesandt. <br>";
	  echo "<form action=UebCookie_Seite2.php >     <input type=submit value=Weiter gehts /> </form> ";
	  echo "</p> ";
	  }

?>

<hr>
<p>
Some tech infos: <br>
Local var: <?php echo "input_is_valid=" . ($input_is_valid ? "true" : "false") ; ?> <br>
Get vars: <?php echo print_r( var_dump($_GET));?> <br>
Post vars: <?php echo print_r( var_dump($_POST));?> <br>
Cookies: <?php echo print_r($_COOKIE); ?>

</p>

</body>
</html>