<?php
// Start the session
session_start();


// define session variables and initialize with empty values
$_SESSION["fName"] = "";
$_SESSION["lName"] = "";

// local var. not in session:
$input_is_valid = false;


// if present, set the variables to the posted values
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["firstname"])) {
    $_SESSION["fName"] = "Vorame eingeben";
  } else {
    $_SESSION["fName"] = $_POST["firstname"];
  }
  if (empty($_POST["lastname"])) {
    $_SESSION["lName"] = "Name eingeben";
  } else {
    $_SESSION["lName"] = $_POST["lastname"];
  }

// Note: this is a reduced exercise. In a full application, one needs to perform serverside validation!
$input_is_valid = true;
  }


  ?>
<!DOCTYPE html>
<html>
<head>

<title>PHP Uebung mit Session</title>
</head>
<body>
<h1>Eingabeformular</h1>

<p>
Diese Aufgabe zeigt nur den Einsatz von Sessions. F&uuml;r eine Login-Seite oder &Auml;hnliches sind verschiedene weitere Aspekte zu beachten.
<br>
<a href="https://www.w3schools.com/php/php_sessions.asp">https://www.w3schools.com/php/php_sessions.asp</a>
<br>
<a href="https://www.w3schools.com/php/php_form_validation.asp"> https://www.w3schools.com/php/php_form_validation.asp</a>
<br>
Passwort und DB:<a href="https://www.php-einfach.de/experte/php-codebeispiele/loginscript/">https://www.php-einfach.de/experte/php-codebeispiele/loginscript/</a>
</p>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  First name:<br>
  <input type="text" name="firstname" minlength=0 maxlength=50 required value="<?php echo $_SESSION["fName"];?>" ><br>
  Last name:<br>
  <input type="text" name="lastname" minlength=2 maxlength=50 required value="<?php echo $_SESSION["lName"];?>" ><br><br>
  <input type="submit" value="Anmelden">
</form>

<?php
  if ($input_is_valid){
	  echo " <p> ";
	  echo "Mit Weiter geht es auf eine neue Seite. Die eingegebenen Informationen werden nicht erneut an den Server gesandt. <br>";
	  echo "<form action=UebSession_Seite2.php >     <input type=submit value=Weiter gehts /> </form> ";
	  echo "</p> ";
	  }

?>

<hr>
<p>
Some tech infos: <br>
Local var: <?php echo "input_is_valid=" . ($input_is_valid ? "true" : "false") ; ?> <br>
Get vars: <?php echo print_r( var_dump($_GET));?> <br>
Post vars: <?php echo print_r( var_dump($_POST));?> <br>
Session vars: <?php echo print_r( var_dump($_SESSION));?> <br>
Cookies: <?php echo print_r($_COOKIE); ?>
</p>

</body>
</html>