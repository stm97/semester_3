<?php
// Start the session
session_start();
?>
<!DOCTYPE html>
<html>
<head>

<title>PHP Uebung mit Session, Seite 2</title>
</head>
<body>

<h1>
<?php 
echo "Willkommen " . $_SESSION["fName"] . " " . $_SESSION["lName"] ; 
?>
</h1>

<p>
Vorname und Name wurden aus der serverseitigen Session gelesen.
</p>

<hr>
<p>
Some tech infos: <br>
Get vars: <?php echo print_r( var_dump($_GET));?> <br>
Post vars: <?php echo print_r( var_dump($_POST));?> <br>
Session vars: <?php echo print_r( var_dump($_SESSION));?> <br>

</p>

</body>
</html>