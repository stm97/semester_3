<!DOCTYPE html>
<html>
<head>

<title>PHP Uebung mit Cookies, Seite 2</title>
</head>
<body>

<h1>
<?php 
echo "Willkommen " . $_COOKIE["fiName"] . " " . $_COOKIE["laName"] ; 
?>
</h1>

<p>
Vorname und Name wurden serverseitig aus dem Cookie gelesen. <br>
Kleiner Test: Warten Sie 20 Sekunden und klicken Sie auf Refresh. Was erwarten Sie? <br>
Zweiter Test: Schliessen Sie den Browser und öffnen Sie diese Seite erneut. 
</p>

<hr>
<p>
Some tech infos: <br>
Get vars: <?php echo print_r( var_dump($_GET));?> <br>
Post vars: <?php echo print_r( var_dump($_POST));?> <br>
Cookies: <?php echo print_r($_COOKIE); ?>
</p>

</body>
</html>