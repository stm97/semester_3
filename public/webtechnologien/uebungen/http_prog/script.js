let url = 'https://swapi.co/api/people/';
let personen;

fetch(url, {method: 'GET'}).then(response => {
    if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' + response.status);
        return;
    }
    response.json().then(function (data) {
        personen = data.results;
        for (let i of personen) {
            creatCard(i.name, i.birth_year, i.homeworld);
        }
    });
});

function creatCard(titlevar, subtitlevar, homewvar) {
    let divcol = document.createElement('div');
    let divcard = document.createElement('div');
    let divcardbody = document.createElement('div');
    let h4title = document.createElement('h4');
    let h4subtitle = document.createElement('h4');
    let buttonheim = document.createElement('button');

    divcol.className = 'col-sm';
    divcard.className = 'card';
    divcardbody.className = 'card-body';
    h4title.className = 'card-title';
    h4subtitle.className = 'card-subtitle mb-2 text-muted';
    buttonheim.className = 'btn btn-dark';
    buttonheim.addEventListener('click', function () {
        getHome(homewvar, buttonheim);
    });

    h4title.innerHTML = titlevar;
    h4subtitle.innerHTML = subtitlevar;
    buttonheim.innerHTML = 'Heimat anzeigen';

    divcardbody.appendChild(h4title);
    divcardbody.appendChild(h4subtitle);
    divcardbody.appendChild(buttonheim);
    divcard.appendChild(divcardbody);
    divcol.appendChild(divcard);

    document.getElementById('people').appendChild(divcol);
}

function getHome(url, b) {
    let actualbutton = b;
    fetch(url, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
        }
        response.json().then(function (data) {
            let homew = data.name;
            let newh5 = document.createElement("h5");
            newh5.innerHTML = "Heimat: " + homew;
            actualbutton.parentElement.replaceChild(newh5, actualbutton);
        })
    });
}